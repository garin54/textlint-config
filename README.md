# textlint

日本語(だけじゃなく自然言語の)静的検知ツール。
textlint 本体にプラグイン形式でルールセットを読み込んで解析する。
の個人用設定ファイル集。

詳細な使い方は本家を確認してください。

* https://textlint.github.io/
* https://github.com/textlint/textlint
* https://efcl.info/2015/09/10/introduce-textlint/

## ルールセット

textlint の日本語コミュニティが提供しているルール
https://github.com/textlint-ja

グローバル
https://github.com/textlint/textlint/wiki/Collection-of-textlint-rule

技術文章向け
https://github.com/textlint-ja/textlint-rule-preset-ja-technical-writing
http://neos21.hatenablog.com/entry/2017/11/05/080000

表記ゆれ
https://github.com/textlint-rule/textlint-rule-prh


## インストール

### 本体
``` console
// 本体
$ sudo npm install -g textlint
```

### ルール

各ルールの説明は  dot.textlintrc.yml を参照。

``` console
// ルールプラグイン
$ sudo npm install -g textlint-rule-preset-japanese \
textlint-rule-preset-ja-spacing \
textlint-rule-preset-ja-technical-writing \
textlint-rule-ja-no-abusage \
textlint-rule-no-mixed-zenkaku-and-hankaku-alphabet \
textlint-rule-ja-hiragana-fukushi \
textlint-rule-ja-hiragana-hojodoushi \
textlint-rule-ja-no-redundant-expression \
textlint-rule-ja-unnatural-alphabet \
textlint-rule-joyo-kanji \
textlint-rule-no-renyo-chushi \
textlint-rule-one-white-space-between-zenkaku-and-hankaku-eiji \
textlint-rule-period-in-list-item \
@textlint-ja/textlint-rule-no-insert-dropping-sa \
textlint-rule-max-appearence-count-of-words
```

### フィルタ

対象外とする Markdown のノードを指定したり、対象外の文字列(正規表現)を指定する filter:設定用のプラグイン。

```
sudo npm install -g textlint-filter-rule-comments textlint-filter-rule-node-types textlint-filter-rule-whitelist
```

* textlint-filter-rule-comments : フィルターコメント
* textlint-filter-rule-node-types : ノードタイプフィルター

### プラグイン

```
sudo npm install -g @textlint/textlint-plugin-markdown
```

### 表記揺れ

表記揺れは prh(https://github.com/prh/prh) をバックエンドに使っている。

```
$ sudo npm install -g prh textlint-rule-prh
```

定義ファイルは別途 .yml ファイルを用意する

``` console
$ mkdir share/conf/
$ cp -r prh share/conf/
```


## 使い方

``` console
$ textlint test.md
```

## 設定

設定ファイルは ~/.textlintrc に置く。JSON or YAML 形式で書く

```console
$ cp dot.textlintrc.yml ~/.textlintrc.yml
```

## チップス

### フィルタ
https://efcl.info/2016/06/30/textlint7.0/

``` markdown
<!-- textlint-disable -->

ここは対象にならない。

<!-- textlint-enable -->
```

whitelit
https://github.com/textlint/textlint-filter-rule-whitelist


### .howm のような拡張子を Markdown として認識させる

textlint はデフォルトで拡張子を見てファイル形式を判断する(.md なら Markdown のように)。
textlint 11 で追加された [extensions オプション](https://efcl.info/2018/07/23/textlint-11/)を使うと変換プラグイン(MarkdownとかText)ごとに対応する拡張子を指定できる。

`/home/yoshino/.textlintrc.yml`
``` yaml
// .howm 拡張子のファイルを textlint-plugin-markdown で検査する
// ※要 @textlint/textlint-plugin-markdown プラグイン
plugins:
  "@textlint/textlint-plugin-markdown":
    extensions:
      - ".howm"
```
